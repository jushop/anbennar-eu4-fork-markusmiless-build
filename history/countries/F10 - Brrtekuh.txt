government = steppe_horde
government_rank = 1
primary_culture = sandfang_gnoll
religion = xhazobkult
technology_group = tech_gnollish
national_focus = DIP
capital = 460

1439.2.1 = {
	monarch = {
		name = "Oglepp"
		dynasty = "Brrtekuh"
		birth_date = 1431.9.7
		adm = 0
		dip = 0
		mil = 0
	}
}