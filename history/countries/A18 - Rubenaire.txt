government = feudal_monarchy
government_rank = 1
primary_culture = high_lorentish
religion = regent_court
technology_group = tech_cannorian
capital = 108 # Rubenaire
national_focus = DIP

1422.1.1 = { set_country_flag = lilac_wars_moon_party }

1441.3.3 = {
	monarch = {
		name = "Ri�n V"
		dynasty = "s�l Rubenaire"
		birth_date = 1424.6.11
		adm = 4
		dip = 3
		mil = 6
	}
}