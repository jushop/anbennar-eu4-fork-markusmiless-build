# No previous file for Hanzhong
owner = F28
controller = F28
add_core = F28
culture = sandfang_gnoll
religion = xhazobkult

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = gold

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_bulwari