# No previous file for Marrhold
owner = B36
controller = B36
add_core = B36
culture = marrodic
religion = regent_court

hre = no

base_tax = 5
base_production = 5
base_manpower = 5

trade_goods = iron

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish