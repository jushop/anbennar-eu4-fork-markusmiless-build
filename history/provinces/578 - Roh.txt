# No previous file for Roh
owner = F05
controller = F05
add_core = F05
culture = sun_elf
religion = bulwari_sun_cult

hre = no

base_tax = 2
base_production = 3
base_manpower = 2

trade_goods = fish

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari