#452 - Gemisle

owner = F17
controller = F17
add_core = F17
culture = crathanori
religion = regent_court


base_tax = 4
base_production = 3
base_manpower = 2

trade_goods = gems

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari