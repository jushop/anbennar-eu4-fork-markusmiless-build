#126 - Malta | Portnamm

owner = A19
controller = A19
add_core = A19

culture = creek_gnome
religion = regent_court

hre = no

base_tax = 9 
base_production = 9
base_manpower = 7

trade_goods = paper

capital = "The City on the Portroy" 

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

add_permanent_province_modifier = {
	name = center_of_trade_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = portroy_estuary_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = human_minority_coexisting_large
	duration = -1
}