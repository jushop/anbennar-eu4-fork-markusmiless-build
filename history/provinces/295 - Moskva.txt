# No previous file for Moskva
owner = A84
controller = A84
add_core = A84
culture = vernman
religion = regent_court

hre = yes

base_tax = 3
base_production = 5
base_manpower = 3

trade_goods = salt

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish