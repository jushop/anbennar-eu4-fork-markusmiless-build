#18 - Lappland | 

owner = A40
controller = A40
add_core = A40
culture = exwesser
religion = regent_court

hre = yes

base_tax = 3
base_production = 2
base_manpower = 2

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
