# No previous file for Chiquitos
culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = cloth

native_size = 24
native_ferocity = 4
native_hostileness = 7