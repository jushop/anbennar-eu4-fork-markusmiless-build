#538 - Azka-Sur

owner = F34
controller = F34
add_core = F34
culture = bahari
religion = bulwari_sun_cult

base_tax = 4
base_production = 4
base_manpower = 5

trade_goods = gold

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

