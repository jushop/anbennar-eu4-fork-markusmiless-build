#400 - Muscat

owner = F07
controller = F07
add_core = F07
culture = desha
religion = mother_akan

hre = no

base_tax = 2
base_production = 2
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
