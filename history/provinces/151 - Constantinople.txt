#151 - Constantinople | Beepeck

owner = A12
controller = A12
add_core = A12
culture = imperial_halfling
religion = regent_court

hre = yes

base_tax = 12
base_production = 9
base_manpower = 17

trade_goods = glass

capital = "The Largest Small City"

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

add_permanent_province_modifier = {
	name = center_of_trade_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = gnomish_minority_coexisting_large
	duration = -1
}

add_permanent_province_modifier = {
	name = human_minority_coexisting_large
	duration = -1
}

add_permanent_province_modifier = {
	name = dwarven_minority_coexisting_small
	duration = -1
}