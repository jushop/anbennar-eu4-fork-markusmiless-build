# No previous file for Guiyang
owner = F27
controller = F27
add_core = F27
culture = firanyan_harpy
religion = the_hunt


base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = grain

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari