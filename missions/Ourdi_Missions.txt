F16_missions_main_slot = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = F16
		
	}
	has_country_shield = yes

	reclaim_ourdia = {
		icon = mission_have_two_subjects
		required_missions = { }
		provinces_to_highlight = {
			province_id = 507
			province_id = 508
			province_id = 511
			province_id = 514
			province_id = 520
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			owns_core_province = 507
			owns_core_province = 508
			owns_core_province = 511
			owns_core_province = 514
			owns_core_province = 520
		}
		effect = {
			crathanor_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			medbahar_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	F16_secure_the_bay = {
		icon = mission_conquer_50_development
		required_missions = { reclaim_ourdia }
		position = 3
		provinces_to_highlight = {
			OR = {
				area = crathanor_area
				area = medbahar_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = { 
			crathanor_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			medbahar_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			add_legitimacy = 10
			add_prestige = 10
			add_country_modifier = {
				name = "ourdi_protected_trade"
				duration = 7300
			}
		}
	}
	F16_establishing_in_bahar = {
		icon = mission_unite_home_region
		required_missions = { F16_secure_the_bay }
		position = 5 
		provinces_to_highlight = {
			OR = {
				area = reuyel_area
				area = tungr_mountain_area
				province_id = 830
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			crathanor_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			medbahar_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			owns_core_province = 830
			830 = { fort_level = 2 }
		}
		effect = {
			add_prestige = 25
			add_country_modifier = {
				name = "ourdi_settlers_stream"
				duration = 7300
			}
		}	
	}
	F16_ourdian_crusade = {
		icon = conquer_5_states
		required_missions = { F16_establishing_in_bahar }
		position = 7 
		provinces_to_highlight = {
			bahar_region
		}
		trigger = {
			bahar_region = {
				type = all
				religion = regent_court
			}
		}
		effect = {
			add_legitimacy = 25
			add_prestige = 50 
			add_country_modifier = {
				name = "ourdi_crusading_zeal"
				duration = 3650
			}
		}
	}
	F16_free_the_bulwari = {
		icon = mission_have_two_subjects 
		required_missions = { F16_ourdian_crusade }
		position = 9
		provinces_to_highlight = {
			area = area_hh0a_area
			OR = {
				area = areahh8a_area
				owned_by = F21
			}
		}
		NOT = { country_or_non_sovereign_subject_holds }
		trigger = {
			NOT = {
				exists = F21
			}
			areahh8a_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			area_hh0a_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			add_legitimacy = 25
			add_prestige = 25
			add_country_modifier = {
				name = "ourdi_favoured_by_corin"
				duration = 7300
			}
		}
	}	
	F16_the_final_push = {
		icon = mission_assemble_an_army
		required_missions = { F16_free_the_bulwari }
		position = 11 
		provinces_to_highlight = {
			OR = {
				province_id = 561 # Bengal Delta
				owned_by = F37
			}
			OR = {
				province_id = 496 # Elizna
				owned_by = F05
			}
		}
		NOT = { country_or_non_sovereign_subject_holds }
		trigger = {
			NOT = {
				exists = F37
				exists = F05
			}
			owns_core_province = 561
			owns_core_province = 496
		}
		effect = {
			add_legitimacy = 50
			add_prestige = 50
			add_country_modifier = {
				name = "ourdi_empire_of_men"
				duration = 7300
			}
		}
	}
}

F16_missions_north_slot = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = F16
	}
	has_country_shield = yes
	
	F16_the_path_to_the_north = {
		icon = mission_unite_home_region
		required_missions = { reclaim_ourdia }
		position = 2
		provinces_to_highlight = {
			province_id = 448
			province_id = 449
			NOT = { 
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			owns_core_province = 448
			owns_core_province = 449
		}
		effect = {
			add_country_modifier = {
				name = "ourdi_colonial_fervor"
				duration = 3650
			}
		}
	}
	F16_colonizing_the_flooded_coast = {
		icon = mission_build_grand_navy
		required_missions = { F16_the_path_to_the_north }
		position = 4
		provinces_to_highlight = {
			province_id = 447
			province_id = 445
			province_id = 451
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			owns_core_province = 447
			owns_core_province = 445
			owns_core_province = 451
		}
		effect = {
			add_country_modifier = {
				name = "ourdi_awakening_the_economy"
				duration = 3650
			}
		}
	}
	F16_securing_the_coast = {
		icon = mission_unite_home_region
		required_missions = { F16_colonizing_the_flooded_coast }
		position = 6
		provinces_to_highlight = {
			area = dreadmire_area
			NOT = {
			country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			dreadmire_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			add_country_modifier = {
				name = "ourdi_bustling_trade"
				duration = 3650
			}
		}
	}
	F16_the_destiny_of_the_dostanorians = {
		icon = conquer_5_states
		required_missions = { F16_securing_the_coast }
		position = 8
		provinces_to_highlight = {
			OR = {
				area = baldostan_area
				owned_by = A59
			NOT = { country_or_non_sovereign_subject_holds = ROOT }
			}
		}
		trigger = {
			OR = {
				A59 = { is_subject_of = ROOT }
				NOT = { exists = A59 }
			}
		}
		effect = {
			add_country_modifier = {
				name = "ourdi_dostanorian_unity"
				duration = 7300
			}
			add_legitimacy = 25
			add_prestige = 25
		}
	}
}

F16_masons_missions = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = F16
	}
	has_country_shield = yes
	
	F16_a_suitable_capital = {
		icon = mission_imperial_conquest
		required_missions = { F16_the_path_to_the_north }
		position = 9
		provinces_to_highlight = {
			province_id = 510
		}
		510 = {
			has_province_modifier = { ruined_castanorian_citadel }
		}
		trigger = {
			510 = {
				has_province_modifier = { castanorian_citadel }
			}
		}
		effect = {
			add_prestige = 20
			add_legitimacy = 20
			add_country_modifier = {
				name = "ourdi_castanorian_lessons"
				duration = 3650
			}
		}
	}
	F16_restore_corveld = {
		icon = mission_build_buildings
		required_missions = { F16_a_suitable_capital }
		position = 12 
		provinces_to_highlight = {
			province_id = 447
		}
		510 = {
			owned_by = ROOT
			has_province_modifier = { ruined_city }
		}
		trigger = {
			510 = {
				owned_by = ROOT
				NOT = { has_province_modifier = ruined_city }
			}
		}
		effect = {
			add_prestige = 20
			add_legitimacy = 20
		}
	}
}

			