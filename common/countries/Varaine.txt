#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 153  194  159 }

revolutionary_colors = { 153  194  159 }

historical_idea_groups = {
	exploration_ideas
	maritime_ideas
	economic_ideas
	offensive_ideas
	expansion_ideas
	quality_ideas	
	administrative_ideas	
	influence_ideas
}

historical_units = {
	cannorian_1_medieval_infantry
	cannorian_1_medieval_knights
	cannorian_5_men_at_arms
	cannorian_9_pike_and_shot
	cannorian_10_gun_knights
	cannorian_12_thorn_formation
	cannorian_14_outriders
	cannorian_15_volley_infantry
	cannorian_18_reformed_outriders
	cannorian_19_thorn_formation2
	cannorian_23_line_infantry
	cannorian_26_carabiners
	cannorian_26_dovecoat_infantry
	cannorian_28_impulse_infantry
	cannorian_28_cuirassier
	cannorian_30_drill_infantry
}

monarch_names = {
	#Generic Anbennarian Name List
	"Adelar #0" = 1
	"Ademar #0" = 1
	"Adran #0" = 1
	"Adrian #0" = 1
	"Adrien #0" = 1
	"Alain #0" = 1
	"Albert #0" = 1
	"Aldred #0" = 1
	"Alfons #0" = 1
	"Alfred #0" = 1
	"Alos #0" = 1
	"Alvar #0" = 1
	"Andrel #0" = 1
	"Ardan #0" = 1
	"Ardor #0" = 1
	"Aril #0" = 1
	"Arman #0" = 1
	"Artorian #0" = 1
	"Artur #0" = 1
	"Aucan #0" = 1
	"Austyn #0" = 1
	"Avery #0" = 1
	"Awen #0" = 1
	"Borian #0" = 1
	"Brandon #0" = 1
	"Brayan #0" = 1
	"Brayden #0" = 1
	"Calas #0" = 1
	"Cast�n #0" = 1
	"Cast�n #0" = 1
	"Cecill #0" = 1
	"Corin #0" = 1
	"Daran #0" = 1
	"Darran #0" = 1
	"Denar #0" = 1
	"Dominic #0" = 1
	"Dustin #0" = 1
	"Edmund #0" = 1
	"Elran #0" = 1
	"Emil #0" = 1
	"Erel #0" = 1
	"Eren #0" = 1
	"Erlan #0" = 1
	"Evin #0" = 1
	"Frederic #0" = 1
	"Galin #0" = 1
	"Gelman #0" = 1
	"Kalas #0" = 1
	"Laurens #0" = 1
	"Lucian #0" = 1
	"Luciana #0" = 1
	"Marion #0" = 1
	"Maurise #0" = 1
	"Nara #0" = 1
	"Olor #0" = 1
	"Ot� #0" = 1
	"Re�n #0" = 1
	"Riann�n #0" = 1
	"Ricain #0" = 1
	"Ri�n #0" = 1
	"Robin #0" = 1
	"Rogier #0" = 1
	"Sandur #0" = 1
	"Taelar #0" = 1
	"Teagan #0" = 1
	"Thal #0" = 1
	"Thiren #0" = 1
	"Tom�s #0" = 1
	"Trian #0" = 1
	"Tristan #0" = 1
	"Trystan #0" = 1
	"Valen #0" = 1
	"Valeran #0" = 1
	"Varian #0" = 1
	"Varil #0" = 1
	"Varilor #0" = 1
	"Varion #0" = 1
	"Vernell #0" = 1
	"Vincen #0" = 1
	"Willam #0" = 1
	
	"Adeline #0" = -10
	"Adra #0" = -10
	"Alara #0" = -10
	"Aldresia #0" = -10
	"Alina #0" = -10
	"Alisanne #0" = -10
	"Amarien #0" = -10
	"Amina #0" = -10
	"Arabella #0" = -10
	"Aria #0" = -10
	"Athana #0" = -10
	"Aucanna #0" = -10
	"Bella #0" = -10
	"Calassa #0" = -10
	"Cast�nnia #0" = -10
	"Cast�na #0" = -10
	"Cecille #0" = -10
	"Cela #0" = -10
	"Celadora #0" = -10
	"Clarimonde #0" = -10
	"Constance #0" = -10
	"Cora #0" = -10
	"Coraline #0" = -10
	"Corina #0" = -10
	"Eil�s #0" = -10
	"Eil�sabet #0" = -10
	"El�anore #0" = -10
	"Emil�e #0" = -10
	"Erela #0" = -10
	"Erella #0" = -10
	"Galina #0" = -10
	"Galinda #0" = -10
	"Gis�le #0" = -10
	"Isabel #0" = -10
	"Isabella #0" = -10
	"Isobel #0" = -10
	"Kerstin #0" = -10
	"Laurenne #0" = -10
	"Lianne #0" = -10
	"Madal�in #0" = -10
	"Margery #0" = -10
	"Maria #0" = -10
	"Mariana #0" = -10
	"Marianna #0" = -10
	"Marianne #0" = -10
	"Marien #0" = -10
	"Marina #0" = -10
	"Re�nna #0" = -10
	"Sofia #0" = -10
	"Sofie #0" = -10
	"Sybille #0" = -10
	"Thalia #0" = -10
	"Valence #0" = -10
	"Varina #0" = -10
	"Varinna #0" = -10
	"Willamina #0" = -10
	"Lisolette #0" = -10
}

leader_names = {
	d'Ambleteuse d'Aulnay
	"de Beauharnais" "de Bercy" "de B�thune" "de Boissieu" "de Bonnefoy" "de Bonneuil" "du Bosquet" "de la Bretonni�re" "de Broglie"
	"de Castaing" "de Caumartin" "de Chambly" "de Chambronne" "de Champmartin" "de Chevigny" "des Coulons" "de Cr�vec�ur" "de Crussol"
	"de Dampierre" Desgouttes Duquesne
	"des �cures" d'Entraigues d'Estr�es
	"du Fournay"
	"de Galard"
	"des Herbiers"
	"de La Barthe" "de La Ferrandie" "de La Galissoni�re" "de La Mothe" "de La Motte d'Airan" "de La Porte" "Le Tellier" "de Lh�ry" "de Luynes"
	"de Marguerye" "de Maurepas"
	"de Neufch�tel"
	"d'Ornano"
	"de Pellefort"
	"de Richemont" "de Rochechouart" "de Rochemaure" "de Rieu"
	"de Saint-Chamond" "de Saint-Esprit" "de Saint-Germain" "de Saint-Omer" "de Siorac"
	"de Tocqueville" "de La Tour d'Auvergne" "de Tr�ville"
	"d'Usson"
	"du Valentinois" "de Vergennes" "de Vibien" "de Vigny" "de Villeneuve"
}

ship_names = {
	#Generic Cannorian
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear Beaver "Black Galley" "Black Pinnace" "Black Prince" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Elephant Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful President Prince "Prince Consort" "Prince Royal"
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite "Young Prince" Zealous

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor Count Lord Baron

	Knight Paladin Dragonslayer Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
	
	#Elven
	Lunetine Agraseina 
	
	#Regent Court Deities
	Castellos Dame "The Dame" Halanna Ysh Yshtralania Agradls Adean Esmaryal Ryala Edronias Falah Nerat Ara Minara Munas Moonsinger Nathalyne Begga Corin Balgar
	Uelos Drax'os
}

army_names = {
	"Varain�y Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Varain�y Fleet" "Bay Squadron" "Dameshead Squadron" "Damescrown Squadron" "Damesneck Squadron" "Moon Squadron" "Singer Squadron"
}